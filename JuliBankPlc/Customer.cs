﻿using System;
using System.Collections.Generic;

namespace JuliBankPlc
{
    public class Customer
    {
        private string CustomerName { get; set; }
        private string AccountNumber { get; set; }
        private decimal Balance { get; set; } = decimal.Zero;

        private List<Transaction> TransactionsHistory { get; set; }

        public Customer(string customerName, string accountNumber)
        {
            CustomerName = customerName;
            AccountNumber = accountNumber;
            TransactionsHistory = new List<Transaction>();
        }

        public List<Transaction> GetCustomerTransactionHistory()
        {
            return TransactionsHistory;
        }

        public decimal GetBalance()
        {
            return Balance;
        }

        public override string ToString()
        {
            return $"Account name: {CustomerName}, Account number: {AccountNumber}";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amount"></param>
        /// <returns>The amount remaining in the balance</returns>
        public decimal Withdraw(decimal amount)
        {
            if(amount > Balance)
            {
                throw new Exception("Withdrawal amount is greater than balance");
            }
            Balance -= amount;
            Transaction withdrawalTransaction = new Transaction {
                AccountNumber = AccountNumber,
                Amount = amount,
                Timestamp = DateTime.UtcNow,
                Type = TransactionType.WITHDRAW
            };
            TransactionsHistory.Add(withdrawalTransaction);
            return Balance;
        }


        public decimal Deposit(decimal amount)
        {
            if(amount <= decimal.Zero)
            {
                throw new Exception("Deposit amount cannot be zero or less");
            }
            Balance += amount;
            Transaction depositTransaction = new Transaction
            {
                AccountNumber = AccountNumber,
                Amount = amount,
                Timestamp = DateTime.UtcNow,
                Type = TransactionType.DEPOSIT
            };
            TransactionsHistory.Add(depositTransaction);
            return Balance;
        }
    }
}
