﻿using System;
using System.Collections.Generic;

namespace JuliBankPlc
{
    class Program
    {
        static void Main(string[] args)
        {
            Bank bank = new Bank("Juli Bank PLC");
            Console.Write(" Enter new command: ");
            string userInput = Console.ReadLine();

            while (true)
            {
                try
                {
                    CommandExecutor(bank, userInput.ToUpper());
                    Console.Write(" Enter new command: ");
                    userInput = Console.ReadLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.Write(" Enter new command: ");
                    userInput = Console.ReadLine();
                }
            }

        }

        static void CommandExecutor(Bank bank,string userInput)
        {
            switch (userInput)
            {
                case "WITHDRAW":
                    Console.Write("Account number: ");
                    string withdrawAccountNumber = Console.ReadLine();
                    Console.Write("Amount: ");
                    string withdrawAmount = Console.ReadLine();
                    decimal amount = decimal.Parse(withdrawAmount);
                    decimal balance = bank.WithdrawMoney(withdrawAccountNumber, amount);
                    Console.WriteLine($"Balance: {balance}");
                    break;

                case "DEPOSIT":
                    Console.Write("Account number: ");
                    string depositAccountNumber = Console.ReadLine();
                    Console.Write("Amount: ");
                    string depositAmount = Console.ReadLine();
                    decimal damount = decimal.Parse(depositAmount);
                    decimal dbalance = bank.DepositMoney(depositAccountNumber, damount);
                    Console.WriteLine($"Balance: {dbalance}");
                    break;
                case "LIST CUSTOMERS":
                    List<Customer> customers = bank.GetAllCustomers();
                    foreach(Customer customer in customers)
                    {
                        Console.WriteLine(customer);
                    }
                    break;
                case "ENROLL CUSTOMER":
                    Console.Write("Account name: ");
                    string newAccountName = Console.ReadLine();
                    Console.Write("Account number: ");
                    string newUserAccount = Console.ReadLine();
                    string newCustomerDetails = bank.EnrollCustomers(newAccountName, newUserAccount);
                    Console.WriteLine(newCustomerDetails);
                    break;
                case "ALL TRANSACTIONS":
                    List<Transaction> allTransactions = bank.GetAllTransations();
                    foreach(Transaction transaction in allTransactions)
                    {
                        Console.WriteLine(transaction);
                    }
                    break;
                case "USER TRANSACTION":

                    Console.Write("Account name: ");
                    string transAccountNumber = Console.ReadLine();
                    List<Transaction> userTransactions = bank.GetUserTransactions(transAccountNumber);
                    foreach (Transaction transaction in userTransactions)
                    {
                        Console.WriteLine(transaction);
                    }
                    break;
                case "BALANCE":
                    Console.Write("Account name: ");
                    string balanceAccountNumber = Console.ReadLine();
                    decimal userBalance =  bank.UserBalance(balanceAccountNumber);
                    Console.WriteLine(userBalance);
                    break;
                default:
                    Console.WriteLine("Unknown command entered");
                    break;
            }
        }
    }
}
