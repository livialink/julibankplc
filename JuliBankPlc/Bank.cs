﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JuliBankPlc
{
    public class Bank
    {
        private Dictionary<string,Customer> Accounts { get; set; }
        private string BankName { get; set; }
        public Bank(string bankName)
        {
            BankName = bankName;
            Accounts = new Dictionary<string, Customer>();
        }
        public override string ToString()
        {
            return $"Bank : {BankName}";
        }
        /// <summary>
        /// Deposits money into the specified customer account and returns the balance 
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="amount"></param>
        /// <returns>The new customer balance</returns>
        public decimal DepositMoney(string accountNumber, decimal amount)
        {
            if (!Accounts.TryGetValue(accountNumber, out Customer foundCustomer))
            {
                //throw exception since customer does not exist
                throw new Exception($"Customer with account number {accountNumber} does not exist ");
            }
           return  foundCustomer.Deposit(amount);
        }
        public decimal UserBalance(string accountNumber)
        {
            if (!Accounts.TryGetValue(accountNumber, out Customer foundCustomer))
            {
                //throw exception since customer does not exist
                throw new Exception($"Customer with account number {accountNumber} does not exist ");
            }
            return foundCustomer.GetBalance();
        }
        /// <summary>
        /// Withdraws money from the specified customer account and return the balance
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public decimal WithdrawMoney(string accountNumber, decimal amount)
        {
            if (!Accounts.TryGetValue(accountNumber, out Customer foundCustomer))
            {
                //throw exception since customer does not exist
                throw new Exception($"Customer with account number {accountNumber} does not exist ");
            }
            return foundCustomer.Withdraw(amount);
        }
        public string EnrollCustomers(string customerFullName, string accountNumber)
        {
            Customer newCustomer = new Customer(customerFullName, accountNumber);
            bool isAccountAdded = Accounts.TryAdd(accountNumber, newCustomer);
            if (!isAccountAdded)
            {
                throw new Exception("Account creation failed. Possible duplicate account number");
            }
            return newCustomer.ToString();
        }
        /// <summary>
        /// Get all the customers
        /// </summary>
        /// <returns></returns>
        public List<Customer> GetAllCustomers()
        {
            return Accounts.Values.ToList();
        }
        /// <summary>
        /// Get the transaction history of all the customers
        /// </summary>
        /// <returns></returns>
        public List<Transaction> GetAllTransations()
        {
            List<Transaction> allTransaction = new List<Transaction>();
            foreach(var account in Accounts)
            {
                allTransaction.AddRange(account.Value.GetCustomerTransactionHistory());
            }
            return allTransaction;
        }

        /// <summary>
        /// Get individual customer transaction history
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        public List<Transaction> GetUserTransactions(string accountNumber)
        {
            
            if(!Accounts.TryGetValue(accountNumber, out Customer foundCustomer))
            {
                //throw exception since customer does not exist
                throw new Exception($"Customer with account number {accountNumber} does not exist ");
            }

            return foundCustomer.GetCustomerTransactionHistory();
        }
    }
}
