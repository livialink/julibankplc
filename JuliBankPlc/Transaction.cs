﻿using System;
namespace JuliBankPlc
{
    public class Transaction
    {
        public TransactionType Type { get; set; }
        public decimal Amount { get; set; }
        public string AccountNumber { get; set; }
        public DateTime Timestamp { get; set; }

        public override string ToString()
        {
            return $"{AccountNumber}: {Type}, {Amount}, {Timestamp}";
        }
    }

    public enum TransactionType
    {
        DEPOSIT,
        WITHDRAW
    }
   
}
